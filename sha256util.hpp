#ifndef SHA256UTIL_HPP
#define SHA256UTIL_HPP

#include <openssl/sha.h>

#include <array>
#include <stdexcept>
#include <string>

namespace tom::hasher {

struct SHA256BuildError : public std::runtime_error {
    explicit SHA256BuildError(std::string const& s);
};

struct SHA256Hash {
    std::array<unsigned char, SHA256_DIGEST_LENGTH> data;

    [[nodiscard]] std::string hex() const;

    static SHA256Hash ofString(std::string const& s);

    static SHA256Hash ofFile(std::string const& filename);
};

template <class T>
concept HasCharData = requires(T t) {
    { t.data() } -> std::same_as<char*>;
};

struct SHA256Builder {
private:
    SHA256_CTX ctx {};
    bool invalid_ {};
    std::size_t bytesWritten_ {};

public:
    SHA256Builder();

    SHA256Builder(SHA256Builder const&);
    SHA256Builder(SHA256Builder&&);

    SHA256Builder& operator=(SHA256Builder const&);
    SHA256Builder& operator=(SHA256Builder&&);

    ~SHA256Builder() = default;

    void update(HasCharData auto const& s, std::size_t len)
    {
        if (invalid_)
            throw std::runtime_error("Builder was already finalized");
        auto i = SHA256_Update(&ctx, s.data(), len);
        bytesWritten_ += len;
        if (i == 0)
            throw std::runtime_error("Could not perform SHA256 update");
    }

    void update(char* data, std::size_t length);

    std::size_t bytesWritten() const noexcept;

    bool isValid() const noexcept;

    SHA256Hash finish();
};

} // namespace tom::hasher
#endif
