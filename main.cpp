#include "betterfile.hpp"
#include "sha256util.hpp"
#include <concepts>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <ios>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

#define STRING_IMPL(x) #x
#define STRING(x) STRING_IMPL(x)
#define LSTR STRING(__LINE__)

#ifndef FILE_READ_BUF_SIZE
#define FILE_READ_BUF_SIZE 65536
#endif

struct arguments {
    std::vector<std::string> filenames;
    std::vector<std::string> strings;
};

struct Color {
    std::string const ansi_escape_sequence;
    std::string const name;

    static Color BOLD;
    static Color DIM;
    static Color ITALIC;
    static Color UNDERLINE;
    static Color RED;
    static Color GREEN;
    static Color YELLOW;
    static Color BLUE;
    static Color MAGENTA;
    static Color CYAN;
    static Color WHITE;
    static Color BLACK;
    static Color RESET;

    operator std::string() { return ansi_escape_sequence; }

    Color operator|(Color const& other) const { return (*this) + other; }

    Color operator+(Color const& other) const
    {
        return Color { ansi_escape_sequence + other.ansi_escape_sequence,
            name + " " + other.name };
    }

    inline static std::string reset_escape_sequence() { return "\033[0m"; }
};

std::ostream& operator<<(std::ostream& os, Color color)
{
    os << color.ansi_escape_sequence;
    return os;
}

Color Color::BOLD { "\033[1m", "bold" };
Color Color::DIM { "\033[2m", "dim" };
Color Color::ITALIC { "\033[3m", "italic" };
Color Color::UNDERLINE { "\033[4m", "underline" };
Color Color::RED { "\033[31m", "red" };
Color Color::GREEN { "\033[32m", "green" };
Color Color::YELLOW { "\033[33m", "yellow" };
Color Color::BLUE { "\033[34m", "blue" };
Color Color::MAGENTA { "\033[35m", "magenta" };
Color Color::CYAN { "\033[36m", "cyan" };
Color Color::WHITE { "\033[37m", "white" };
Color Color::BLACK { "\033[30m", "black" };
Color Color::RESET { "\033[0m", "reset" };

template <class T>
concept Streamable = requires(T value, std::stringstream& os) {
    { os.operator<<(value) } -> std::same_as<std::stringstream&>;
};

inline std::string str(Streamable auto c)
{
    std::stringstream s;
    s << c;
    return s.str();
}

#ifdef NDEBUG

#define debug(...) (void)0
#define cdebug(c, ...) (void)0

#else

template <class T, class... Arguments>
inline constexpr void _debugt_int(std::ostream& os, T first, Arguments... args)
{
    os << first;
    if constexpr (sizeof...(args) >= 1)
        _debugt_int(os, args...);
    else
        os << std::endl;
}
template <class... Arguments>
inline constexpr void _debugt(int line, Color color, std::ostream& os, Arguments... args)
{
    os << color << __FILE__ ":" << line << " " << Color::RESET + color;
    _debugt_int(os, args...);
}

#define debug(...) _debugt(__LINE__, Color::YELLOW, std::cerr, __VA_ARGS__)
#define cdebug(c, ...) _debugt(__LINE__, (c), std::cerr, __VA_ARGS__)

#endif

arguments getargs(int argc, char* const argv[])
{
    arguments result {};
    for (int idx = 1; idx < argc;) {
        debug("Looping... ");
        if (std::strcmp(argv[idx], "-f") == 0) {
            debug("Got filename option with filename ", argv[idx + 1]);
            result.filenames.push_back(argv[idx + 1]);
            if (!std::filesystem::is_regular_file(argv[idx + 1])) {
                throw std::invalid_argument("-f argument is not a file");
            }
            idx += 2;
        } else {
            debug("Got string with value ", argv[idx]);
            result.strings.push_back(argv[idx++]);
        }
    }
    return result;
}

int main(int argc, char* const argv[])
{

    auto args = getargs(argc, argv);

    tom::hasher::SHA256Builder builder {};

    for (auto const& s : args.strings) {
        builder.update(s, s.size());
    }

    for (auto const& path : args.filenames) {
        tom::files::File<char> file(path, std::ios_base::binary);

        for (;;) {
            auto result = file.read(FILE_READ_BUF_SIZE);
            // auto result = file.readAll();

            debug("Read ", result.bytesRead, " bytes from file ", file.path());
            builder.update(result.buffer, result.bytesRead);
            if (file.didReachEof()) {
                debug("Done reading file " + file.path());
                break;
            }
        }
    }

    cdebug(Color::GREEN, "Builder accepted ", builder.bytesWritten(), " bytes from ", args.filenames.size(), " files and ", args.strings.size(), " strings");

    std::cout << builder.finish().hex() << std::endl;
}
