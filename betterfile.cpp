#include "betterfile.hpp"

namespace tom::files {

std::ostream& operator<<(std::ostream& os,
    typename File<char>::ReadResult const& result)
{
    for (std::streamsize i = 0; i < result.bytesRead; i++) {
        os << result.buffer[i];
    }
    return os;
}
} // namespace tom::files
