#ifndef BETTERFILE_HPP
#define BETTERFILE_HPP

#include <cstring>
#include <filesystem>
#include <fstream>
#include <ios>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <string_view>

namespace tom::files {

struct IOError : public std::runtime_error {

    IOError(std::string const& message)
        : std::runtime_error(message)
    {
    }
};

template <class DataType>
class File {
    using DataType_t = DataType;
    std::string path_;
    std::ios::openmode mode_;
    std::basic_ifstream<DataType> stream;
    bool eof;

public:
    static std::streamsize const ALL_BYTES = -1;

    struct ReadResult {
    private:
        void lvalue_become(ReadResult const& other)
        {
            bytesRequested = other.bytesRequested;
            delete[] buffer;
            buffer = new DataType[bytesRequested];
            std::memcpy(buffer, other.buffer, bytesRequested);
            bytesAllocated = other.bytesAllocated;
            bytesRead = other.bytesRead;
        }

        void rvalue_become(ReadResult&& other)
        {
            bytesRequested = other.bytesRequested;
            delete[] buffer;
            buffer = other.buffer;
            other.buffer = nullptr;
            bytesRead = other.bytesRead;
            bytesAllocated = other.bytesAllocated;
            other.bytesRequested = 0;
            other.bytesRead = 0;
        }

    public:
        DataType* buffer {};
        /** This is the number of bytes actually read out of the file. Must be <=
         * bytesAllocated and bytesRequested */
        std::streamsize bytesRead {};
        /** This is the total amount of memory allocated in buffer. The read bytes
         * always start from at the address of buffer and are continuous, going from
         * *(buffer) + *(buffer + bytesRead) but there is not guarantee as to what
         * is stored beyond *(buffer + bytesRead) and the data is NOT NULL
         * terminated*/
        std::streamsize bytesAllocated {};
        /** This is the amount of bytes the user requested to read. A user could
         * request 4096 bytes, and get a buffer of 4096 back, but the file may only
         * contain for example 2000 bytes so bytesRead will only be 2000*/
        std::streamsize bytesRequested {};

        ReadResult(DataType* buffer, std::streamsize bytesRead,
            std::streamsize bytesRequested, std::streamsize bytesAllocated)
            : buffer(buffer)
            , bytesRead(bytesRead)
            , bytesAllocated(bytesAllocated)
            , bytesRequested(bytesRequested)
        {
        }

        ReadResult(ReadResult const& other)
        {
            lvalue_become(other);
        }

        ReadResult(ReadResult&& other)
        {
            rvalue_become(std::move(other));
        }

        ReadResult& operator=(ReadResult& other)
        {
            if (this == &other) {
                return *this;
            }
            lvalue_become(other);
            return *this;
        }

        ReadResult& operator=(ReadResult&& other)
        {
            if (this == &other) {
                return *this;
            }
            rvalue_become(std::move(other));
            return *this;
        }

        std::string_view view() const&
        {
            return std::string_view { buffer,
                static_cast<std::string::size_type>(bytesRead) };
        }

        operator std::string_view() const& { return view(); }

        explicit operator std::string() const { return str(); }

        std::string str() const
        {
            return std::string { buffer,
                static_cast<std::string::size_type>(bytesRead) };
        }

        ~ReadResult() { delete[] buffer; }
    };

    File(std::string const& path, std::ios::openmode mode)
        : path_(path)
        , mode_(mode)
    {
        stream = std::ifstream { path_, mode_ };
    }

    File(File<DataType>& other)
    {
        path_ = other.path_;
        mode_ = other.mode_;
        eof = other.eof;
        stream.close();
        stream = std::ifstream { path_, mode_ };
        stream.seekg(other.stream.tellg());
    };

    File(File<DataType>&& other)
    {
        path_ = other.path_;
        mode_ = other.mode_;
        eof = other.eof;
        stream.close();
        stream = other.stream;

        other.path_ = "";
        other.eof = true;
    }

    File& operator=(File<DataType>& other)
    {
        if (this == &other)
            return *this;

        path_ = other.path_;
        mode_ = other.mode_;
        eof = other.eof;
        stream.close();
        stream = std::ifstream { path_, mode_ };
        stream.seekg(other.stream.tellg());
        return *this;
    };

    File& operator=(File<DataType>&& other)
    {
        if (this == &other)
            return *this;

        path_ = other.path_;
        mode_ = other.mode_;
        eof = other.eof;
        stream.close();
        stream = other.stream;

        other.path_ = "";
        other.eof = true;
        return *this;
    }

    std::streamsize tell() { return stream.tellg(); }

    void seek(std::streamsize s) { stream.seekg(s); }

    ReadResult readchar()
    {
        auto b = new DataType[1];
        *b = stream.get();
        return { b, 1, 1 };
    }

    DataType getch() { return stream.get(); }

    ReadResult read(std::streamsize count)
    {
        if (count == 0) {
            return { nullptr, 0, 0, 0 };
        }
        auto d = new DataType[count];
        stream.read(d, count);
        ReadResult result { d, stream.gcount(), count, count };
        eof = stream.eof() || (stream.gcount() == 0);
        return result;
    }

    ReadResult readAll()
    {
        static std::streamsize const CHUNK_SIZE = 4096;
        auto result = read(CHUNK_SIZE);
        if (result.bytesRead < CHUNK_SIZE && eof) {
            return result;
        }
        std::streamsize total = CHUNK_SIZE;
        std::streamsize capacity = total;
        DataType* reserve = new DataType[total];
        std::memcpy(reserve, result.buffer, result.bytesRead);
        while (!eof) {
            std::cout << "looping" << std::endl;
            auto result = read(CHUNK_SIZE);
            if ((result.bytesRead + total) > capacity) {
                std::streamsize newCap = ((result.bytesRead + total) > (capacity * 2))
                    ? ((result.bytesRead + total) * 3 / 2)
                    : (capacity * 2);
                DataType* newRest = new DataType[newCap];
                std::memcpy(newRest, reserve, total);
                std::memcpy(newRest + total, result.buffer, result.bytesRead);
                delete[] reserve;
                reserve = newRest;
                total = total + result.bytesRead;
                capacity = newCap;
            } else {
                std::memcpy(reserve + total, result.buffer, result.bytesRead);
                total += result.bytesRead;
            }
        }
        return { reserve, total, File<DataType>::ALL_BYTES, capacity };
    }

    void reset()
    {
        stream.clear();
        stream.seekg(0);
        eof = false;
    }

    bool didReachEof() const noexcept { return eof; }
    bool didFail() const noexcept { return stream.fail(); }
    bool isBad() const noexcept { return stream.bad(); }

    std::string const& path() const noexcept { return path_; }

    std::ios::openmode const& mode() const noexcept { return mode_; }

    ~File() { stream.close(); }
};
std::ostream& operator<<(std::ostream& os,
    typename File<char>::ReadResult const& result);

} // namespace tom::files

#endif
