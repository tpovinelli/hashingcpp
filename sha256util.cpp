#include "sha256util.hpp"

#include <openssl/sha.h>

#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>

namespace tom::hasher {

SHA256BuildError::SHA256BuildError(std::string const& s)
    : std::runtime_error(s)
{
}

SHA256Hash SHA256Hash::ofString(std::string const& s)
{
    SHA256Builder builder {};
    builder.update(s, s.size());
    return builder.finish();
}

SHA256Hash SHA256Hash::ofFile(std::string const& filename)
{
    SHA256Builder builder {};
    std::ifstream f { filename, std::ios_base::binary };

    std::array<char, 4096> buf {};
    while (f.good() && !f.eof()) {
        f.read(buf.data(), 4096);
        builder.update(buf, f.gcount());
    }
    return builder.finish();
}

bool SHA256Builder::isValid() const noexcept { return !invalid_; }

std::string SHA256Hash::hex() const
{
    std::stringstream ss;
    ss << std::hex;
    for (int i = 0; i < SHA256_DIGEST_LENGTH; ++i) {
        ss << std::setw(2) << std::setfill('0') << (int)data[i];
    }
    return ss.str();
}

SHA256Builder::SHA256Builder()
{
    int i = SHA256_Init(&ctx);
    if (i == 0) {
        throw SHA256BuildError("Could not perform SHA256 initialization");
    }
}

SHA256Builder::SHA256Builder(SHA256Builder const& other)
{
    std::memcpy(&ctx, &other.ctx, sizeof(other.ctx));
    invalid_ = other.invalid_;
    bytesWritten_ = other.bytesWritten_;
}

SHA256Builder::SHA256Builder(SHA256Builder&& other)
{
    this->ctx = other.ctx;
    invalid_ = other.invalid_;
    bytesWritten_ = other.bytesWritten_;
    other.invalid_ = true;
}

SHA256Builder& SHA256Builder::operator=(SHA256Builder const& other)
{
    std::memcpy(&ctx, &other.ctx, sizeof(other.ctx));
    invalid_ = other.invalid_;
    bytesWritten_ = other.bytesWritten_;
    return *this;
}

SHA256Builder& SHA256Builder::operator=(SHA256Builder&& other)
{
    this->ctx = other.ctx;
    invalid_ = other.invalid_;
    bytesWritten_ = other.bytesWritten_;
    other.invalid_ = true;
    return *this;
}

void SHA256Builder::update(char* s, std::size_t length)
{
    if (invalid_)
        throw std::runtime_error("Builder was already finalized");
    auto i = SHA256_Update(&ctx, s, length);
    bytesWritten_ += length;
    if (i == 0)
        throw std::runtime_error("Could not perform SHA256 update");
}

std::size_t SHA256Builder::bytesWritten() const noexcept
{
    return bytesWritten_;
}

SHA256Hash SHA256Builder::finish()
{
    if (invalid_) {
        throw std::runtime_error("Builder was already finalized");
    }
    std::array<unsigned char, SHA256_DIGEST_LENGTH> a {};
    auto i = SHA256_Final(a.data(), &ctx);
    if (i == 0) {
        throw SHA256BuildError("Could not perform SHA256 finalization");
    }
    invalid_ = true;
    SHA256Hash h {};
    h.data = a;
    return h;
}

} // namespace tom::hasher
